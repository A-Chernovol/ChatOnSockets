

import java.io.IOException;

/*
Класс Authorization отвечает за Вход уже зарегестрированного пользователся или регистрацию нового
 */
public class Authorization {
    ClientThread clientThread;

    public Authorization(ClientThread clientThread) {
        this.clientThread = clientThread;
    }

    //Метот для входа существующего пользователя
    private void signIn() throws IOException {
        while (true) {

            clientThread.sendPublic("<Server>", "Enter your login here!");
            String log = clientThread.getReader().readLine();
            log = log.trim();

            if (log.isEmpty()) {
                clientThread.sendPublic("<Server>", "Incorrect login...");
                continue;
            }
            //Проверка клиент уже онлайн или нет
            if(isLogged(log)){
                clientThread.sendPublic("<Server>", "You already logged in other client");
                continue;
            }

            //Проверка на существования введёного логина в базе
            //Если Логин есть в базе то проверяется пароль
            for (User u : clientThread.getUsers()) {
                if (u.getLogin().equals(log)) {
                    clientThread.sendPublic("<Server>", "Enter your password!");
                    String pass = clientThread.getReader().readLine();
                    pass = pass.trim();
                    if (u.getPassword().equals(pass)) {
                        clientThread.sendPublic("<Server>", "Welcome back " + u.getLogin() + "!!!");
                        //Если Логин и Пароль совпадают то устанавлеваем в текущем потоке клиента Его Логин и пароль
                        clientThread.getUser().setLogin(log);
                        clientThread.getUser().setPassword(pass);
                        clientThread.getUser().setLogged(true);
                        return;
                    } else {
                        clientThread.sendPublic("<Server>", "Invalid login or password. Try again!");
                        break;
                    }
                }
            }
            clientThread.sendPublic("<Server>", "Such login doesn't exist...");
        }
    }

    //Метод для регистрации нового пользователя
    private void registration() throws IOException {
        while (true) {
            clientThread.sendPublic("<Server>", "Enter your login here!");
            String log = clientThread.getReader().readLine();
            log = log.trim();

            if (log.isEmpty()) {
                clientThread.sendPublic("<Server>", "Incorrect login...");
                continue;
            }

            //Проверка на уникальность Логина
            if (isExist(log)) {
                clientThread.sendPublic("<Server>", "Such login already exist...");
                continue;
            }

            clientThread.sendPublic("<Server>", "Enter your password!");
            String pass = clientThread.getReader().readLine();
            pass = pass.trim();
            //Устанавлеваем в текущем потоке клиента Его Логин и пароль
            clientThread.getUser().setLogin(log);
            clientThread.getUser().setPassword(pass);
            clientThread.getUser().setLogged(true);

            //Добавляем нового пользователя в базу пользователей
            clientThread.getUsers().add(clientThread.getUser());
            //Сохраняем базу клиентов в файл
            clientThread.getUst().saveUsers();

            clientThread.sendPublic("<Server>", "Thanks for registration " + clientThread.getUser().getLogin() + "!!!");
            return;
        }
    }

    //Метод проверки уникальности логина
    private boolean isExist(String log) {
        for (User u : clientThread.getUsers()) {
            if (u.getLogin().equals(log)) {
                return true;
            }
        }
        return false;
    }
    //Проверка клиент уже онлайн или нет
    private boolean isLogged(String log) throws IOException {
        for (ClientThread cT : clientThread.getClientThreads()) {
            if (cT.getUser().getLogin().equals(log)) {
                return true;
            }
        }
        return false;
    }

    //Регистрация нового пользователя или Вход
    public void signOrReg() throws IOException {
        while (true) {
            clientThread.sendPublic("<Server>", "You already registered? Y/N");
            String answer = clientThread.getReader().readLine();
            if (answer.equals("Y") || answer.equals("y")) {
                signIn();
                break;
            } else if (answer.equals("N") || answer.equals("n")) {
                registration();
                break;
            } else {
                clientThread.sendPublic("<Server>", "Incorrect answer...");
            }
        }
    }
    //Ошраничение подключений
    public boolean isFull() throws Exception {
        if(clientThread.getClientThreads().size() > Server.MAX_CLIENTS_COUNT){
            clientThread.sendPublic("<Server>", "Sorry server is full =(. Please press enter to exit");
            clientThread.getWriter().append("exitServer").append('\n').flush();
            clientThread.close();
            return true;
        }
        return  false;
    }
}
