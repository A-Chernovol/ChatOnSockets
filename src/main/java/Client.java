import java.io.IOException;
import java.net.Socket;
/*
Класс Client ответственный за клиентскую часть приложения.
Создаёт 2 потока.
Console Printer считывает информацию которая пришла с сервера и выводит её в консоль клиента
Console Reader считывает информацию с консоли клиента для её считывания сервером
 */
public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
        try (Socket socket = new Socket("localhost", 8080)) {
            Thread consolePrinter = new Thread(new ConsolePrinter(socket));//Поток вывода в консоль
            Thread consoleReader = new Thread(new ConsoleReader(socket));//Поток считывания с консоли

            consolePrinter.setName("Console printer");
            consolePrinter.setDaemon(true);
            consolePrinter.start();

            consoleReader.setName("Console reader");
            consoleReader.setDaemon(true);
            consoleReader.start();

            consoleReader.join();
            consolePrinter.join();
        }
    }
}
