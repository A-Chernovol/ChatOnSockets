

import java.io.*;
import java.net.Socket;
import java.util.List;
//Класс для работы с клиентом
public class ClientThread implements Runnable, AutoCloseable {
    private final User user;
    private final List<User> users;
    private final List<ClientThread> clientThreads;
    private final BufferedWriter writer;
    private final BufferedReader reader;
    private final UsersStorageTxt ust;
    private final Authorization authorization;
    private final MessageQueue messageQueue;

    public User getUser() {
        return user;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<ClientThread> getClientThreads() {
        return clientThreads;
    }

    public BufferedWriter getWriter() {
        return writer;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public UsersStorageTxt getUst() {
        return ust;
    }

    public ClientThread(List<ClientThread> clientThreads, Socket socket, UsersStorageTxt ust,MessageQueue messageQueue) throws IOException {
        this.users = ust.getUsers();
        this.user = new User();
        this.ust = ust;
        this.clientThreads = clientThreads;
        this.authorization = new Authorization(this);
        this.messageQueue = messageQueue;
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    @Override
    public void run() {
        try {
            if (!authorization.isFull()){
                authorization.signOrReg();
                while (true) {
                    String inputText = reader.readLine();
                    messageQueue.addMessage(new Message(this,inputText));
                }
            }
        } catch (Exception e) {
            try {
                close();
            } catch (Exception ignored) {
            }
            throw new RuntimeException("Connection lost");
        }
    }
    //Формат сообщения для публичной отправки
    public void sendPublic(String login, String text) throws IOException {
        this.writer.append('<').append(login).append("> : ").append(text).append('\n').flush();
    }
    //Формат сообщения для приватной отправки
    public void sendPrivate(ClientThread clientThread, String text) throws IOException {
        this.writer.append("private from ")
                .append('<').append(clientThread.user.getLogin()).append("> : ")
                .append(text).append('\n').flush();
        clientThread.writer.append("private to ")
                .append('<').append(this.user.getLogin()).append("> : ")
                .append(text).append('\n').flush();
    }
    @Override
    public void close() throws Exception {
        clientThreads.remove(this);
        writer.close();
        reader.close();
    }
}
