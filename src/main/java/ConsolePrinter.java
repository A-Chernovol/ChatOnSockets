

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ConsolePrinter implements Runnable, AutoCloseable {
    BufferedReader reader;
    boolean closed;

    public ConsolePrinter(Socket socket) throws IOException {
        closed = false;
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public void run() {
        try {
            while (true) {
                if(closed){
                    close();
                    break;
                }
                String inputText = reader.readLine();
                if (inputText == null || inputText.isEmpty()){
                    Thread.sleep(500);
                }
                // exitServer приходит с сервера и указывает на то что клиент решил выйти с программы
                // Сделал так для того что бы сервер корректно всё закончил и после этого уже закрывается клиент
                if (inputText != null && inputText.equals("exitServer")) {
                    closed = true;
                } else {
                    System.out.println(inputText);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }
}
