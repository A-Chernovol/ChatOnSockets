

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ConsoleReader implements Runnable, AutoCloseable{
    BufferedWriter writer;
    boolean closed;

    public ConsoleReader(Socket socket) throws IOException {
        closed = false;
        this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        try {
            while (true) {
                if (closed){
                    close();
                    break;
                }
                String outputText = scanner.nextLine();
                writer.append(outputText).append('\n').flush();

                if (outputText.equals("exit")) {
                    Thread.sleep(1000);
                    closed = true;
                }
            }
        } catch (Exception ignored) {
        }
    }
    @Override
    public void close() throws Exception {
        writer.close();
    }
}
