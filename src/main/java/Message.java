//Класс для хранения сообщения и потока клиента который это сообщение написал
public class Message {
    ClientThread clientThread;
    String message;

    public Message(ClientThread clientThread, String message) {
        this.clientThread = clientThread;
        this.message = message;
    }
}

