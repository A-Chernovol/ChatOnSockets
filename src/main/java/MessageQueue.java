

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
//Класс для хранения сообщений от клиентов
public class MessageQueue {

    Deque<Message> messages = new ConcurrentLinkedDeque<>();

    public synchronized void addMessage(Message message){
        messages.add(message);
        notifyAll();
    }

    public synchronized Message getMessage() throws InterruptedException {
        while (messages.isEmpty()){
            wait();
        }
        return messages.poll();
    }
}
