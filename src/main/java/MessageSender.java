
//Класс для отправки сообщений клиентам
public class MessageSender implements Runnable {
    MessageQueue messageQueue;
    Message message;

    public MessageSender(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                privateOrPublic();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    //Метод для отправки сообщения
    //Публично или приватно
    private void privateOrPublic() throws Exception {
        message = messageQueue.getMessage();

        //Если сообщение exit то отключает пользователя от сервера
        if (message.message.equals("exit")) {
            message.clientThread.sendPublic("<Server>", "Goodbye " + message.clientThread.getUser().getLogin());
            message.clientThread.getWriter().append("exitServer").append('\n').flush();
            message.clientThread.close();
            return;
        }

        if (message.message.isEmpty())
            return;
        //Если в сообщении @логин то это личное сообщение
        if (message.message.indexOf('@') == -1) {
            for (ClientThread clientThread : message.clientThread.getClientThreads()) {
                if (clientThread.getUser().isLogged())
                    clientThread.sendPublic(message.clientThread.getUser().getLogin(), message.message);
            }
        } else {
            String target = message.message.substring(message.message.indexOf('@') + 1, message.message.indexOf(' '));
            for (ClientThread clientThread : message.clientThread.getClientThreads()) {
                if (clientThread.getUser().getLogin().equals(target)) {
                    clientThread.sendPrivate(message.clientThread, message.message.substring(message.message.indexOf(' ') + 1));
                    break;
                }
            }
        }
    }
}
