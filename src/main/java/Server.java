

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

//Класс Server ответственный за подключение новых пользователей и старт приложения
public class Server {
    public static final int MAX_CLIENTS_COUNT = 20;
    public static void main(String[] args) throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(8080)) {

            //Создание коллекции для потоков клиентов и объект для работы с файлом базы
            List<ClientThread> clientThreads = new ArrayList<>();
            UsersStorageTxt ust = new UsersStorageTxt();

            //Объекты для работы с сообщениями от клиентов
            MessageQueue messageQueue = new MessageQueue();
            MessageSender messageSender = new MessageSender(messageQueue);

            //Создание потока для работа с сообщениями от клиентов
            Thread messageSenderThread = new Thread(messageSender);
            messageSenderThread.setName("messageSender");
            messageSenderThread.setDaemon(true);
            messageSenderThread.start();

            while (true) {
                Socket socket = serverSocket.accept();
                ClientThread newUser = new ClientThread(clientThreads,socket,ust,messageQueue);
                Thread newUserThread = new Thread(newUser);
                newUserThread.setDaemon(true);
                newUserThread.start();

                clientThreads.add(newUser);
            }
        }
    }
}

