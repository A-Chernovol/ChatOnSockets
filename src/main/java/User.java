

import java.io.*;
//Класс пользователя
public class User {
    private String login;
    private String password;
    private boolean logged;

    public User() throws IOException {
        this.logged = false;
        this.login = "";
        this.password = "";
    }

    public User(String login, String password){
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogged() {
        return logged;
    }
    public void setLogged(boolean logged) {
        this.logged = logged;
    }

}
