import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
//Класс для работы с файлом и сохранения зарегестрированных пользователей в файл
public class UsersStorageTxt implements Formatted {
    private final File file;
    private List<User> users;

    public UsersStorageTxt() throws IOException {
        this.file = new File("Users.txt");
        //Создаёт файл если его нет
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    //Создаёт пустую коллекцию или загружает данные из файла в коллекцию и возвращает её
    public List<User> getUsers() {
        if (!file.exists())
            return new ArrayList<>();

        this.users = new ArrayList<>();
        List<String> lines = getLines();
        for (String s : lines) {
            String[] words = s.split(" ");
            users.add(new User(words[0], words[1]));
        }
        return users;
    }

    //Метод сохранения пользователей в файл
    public void saveUsers() throws IOException {
        try (OutputStream writer = new FileOutputStream(file, false)) {
            writer.write(getFormatted().getBytes(StandardCharsets.UTF_8));
            writer.flush();
        }
    }

    //Метод считывания информации с файла
    private List<String> getLines() {
        List<String> list = new ArrayList<>();
        String str;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((str = reader.readLine()) != null) {
                list.add(str);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    //Метод форматирования перед записью в файл
    @Override
    public String getFormatted() {
        StringBuilder sb = new StringBuilder();
        for (User u : users) {
            sb.append(u.getLogin()).append(' ').append(u.getPassword()).append('\n');
        }
        return sb.toString();
    }
}
